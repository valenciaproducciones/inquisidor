<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('home');});
//Route::get('/', function () {return view('expectativa');});
Route::get('/almanza', function () {return view('almanza');});
Route::get('/bardot', function () {return view('bardot');});
Route::get('/erazo', function () {return view('erazo');});
Route::get('/kodak', function () {return view('kodak');});

Route::get('/gato', function () {return view('gato');});
Route::get('/jair', function () {return view('jair');});
Route::get('/restrepo', function () {return view('restrepo');});
Route::get('/lucia', function () {return view('lucia');});

Route::get('/personajes', function () {return view('personajes');});
Route::get('/capitulos', function () {return view('capitulos');});

