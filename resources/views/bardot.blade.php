@extends('layout')
@section('styles')
    @yield('styles')
@endsection

@section('content')
					<div class="row menu justify-content-center">
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/almanza"><b>rOBERTO ALMANZA</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/bardot">AURA BARDOT</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/erazo">ANDRÉS ERAZO </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/kodak">KODAK </a></div>

					<div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/gato"><b>El Gato</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/lucia">Lucía</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/jair">Jair </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/restrepo">Simón Restrepo </a></div>
					</div>
					<div class="row ">
						<div class="col-12 col-md-5">
						<p class="text-left big-text text-center">
						<br>
							AURA BARDOT <div class="red  big-text  text-center" width="75%" >La cazadora de ratas</div>
						</p>
			
						
						</div>
						
						<div class="col-12 col-md-7">
						<br>
						
							<img src="/assets/ab.gif" width="95%"/>
						</div>
					</div>
					<div class= "row">
						<div class="col-12" style="font-size:1.2rem;">
							<br><p>Es una periodista guerrera, de carácter fuerte. Le ha declarado la guerra a la corrupción y tiene el valor de enfrentarse y desenmascarar a los peores políticos del país. Su talón de Aquiles es el amor, sus emociones la traicionan y por eso se enamora de El Gato. Un manipulador. Además, sufre constantes migrañas, que trata de ocultarle a sus compañeros de trabajo.</p>
							
							
						</div> 
					</div> 
@endsection