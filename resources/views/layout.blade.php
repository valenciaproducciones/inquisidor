<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>      
	<meta name="viewport" content="width=device-width, initial-scale=1">	
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);	})(window,document,'script','dataLayer','GTM-KGP6DCD');</script>
        <!-- End Google Tag Manager -->
			
		<link rel="stylesheet" href="{{ mix('css/app.css') }}?v={{ env('APP_VERSION', '0') }}" />
		@yield('styles')
		
		
	
	</head>
	<body class="" id="i">
         <div class="flex-center position-ref full-height">
            <div class="container text-justify" style="border-style:solid; border-width:4px; padding:25px;">
				<div id="date" class="right rider"></div><br>
				<div class="row menu justify-content-center">
                    <div class = "col-12 col-md-4 menuitem text-center"><a href="/">El Inquisidor</a></div>
                    <div class = "col-12 col-md-4 menuitem text-center"><a href="/personajes">Personajes</a></div>
                    <div class = "col-12 col-md-4 menuitem text-center"><a href="/capitulos">Capítulos </a></div>
                </div>
				<div class="row  justify-content-center text-center rider"  style="height:20vh">
					<div class="col-3 col-md-1 small-text"><a href="https://www.rtvcplay.co/series/el-inquisidor" target="blank"><br>Disponible<br>en RTVCPlay<br></a></div>
					<div class="col-6 col-md-9 text-center"><a href="/"><img class="my-auto" src="/assets/logo.png" style="height:20vh; max-width:100%"/></a></div>
					<div class="col-3 col-md-1 small-text"><a href="https://www.rtvcplay.co/series/el-inquisidor" target="blank"><br>Disponible<br>en RTVCPlay<br></a></div>
				</div>
				<div class="row double-border">
				</div>
				@yield('content')
			</div>
		 </div>
		<br>
		<div class= "row text-center"><div class="col-12 col-md-4 " style="font-size:1.2rem;"><div class="fb-like my-2" data-href="https://www.facebook.com/InquisidorSerie/" data-width="200" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div></div><div class="col-12 col-md-8  my-2" style="font-size:1.2rem;">Síguenos &emsp; <a href="https://www.facebook.com/InquisidorSerie/">Facebook</a>  &emsp; <a href="https://www.twitter.com/inquisidorserie/">Twitter</a> &emsp;  <a href="https://www.instagram.com/inquisidorserie/">Instagram</aa> &emsp; <a href="https://www.youtube.com/channel/UCPLEcYZAkzxpTdUUo1QxLgA">Youtube</a></div></div>
		
		<iframe width="100%" height="66" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/680314679&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KGP6DCD"		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
      
		<div id="fb-root"></div>
		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0"></script>
		<script>
		  var d = new Date();
		  var f = d.toLocaleString(window.navigator.language, {weekday: 'long'})+' '+d.toLocaleString(window.navigator.language, {month: 'long'})+' '+d.getDate()+' '+d.getFullYear();
		  document.getElementById("date").innerHTML = f;
		</script>
		@yield('scripts')
	</body>
</html>