@extends('layout')
<html class = "black-body" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>El Inquisidor</title>
                
		
		@yield('styles')
    </head>
    <body 	class = " black-body">
		<div class=" text-center">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="typewriter">
					  <h1 id="frase"></h1>
					</div>

				</div>
			</div>
			<div id="video-header" class="parallax__group">
                         <div class="videocontainer mx-auto">
							<iframe class="video" src="https://www.youtube.com/embed/xlPM6jDb2cM" frameborder="0" allow="autoplay; fullscreen"></iframe>
							
						</div>
					</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="typewriter2" style="color:red">
					  <h1 id="counter" ></h1>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="resp-text" >
					  <br><br>6 de septiembre
					</div>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<iframe width="100%" height="60" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/670161206&color=%23ff1010&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
		</div>
    </body>
	<script>
			var frases=[
				 '"N.N. Nunca nadie sabe nada."'
				,'"Lo contrataron para que<br> invente lo que no sabe."'
				,'"Adiós a los periodistas,<br> que vivan los poetas."'
				,'"La gente compra emociones."'
				,'"El miedo vende más que el odio."'
				,'"Las noticias son como los bebés,<br>nacen en cualquier momento."'
			  	,'"Confunde y reinarás."'
				,'"¿Cuál es la verdadera<br> verdad verdadera?"'
				,'"La verdad no es más que una<br> mentira bien contada."'
				,'"El mayor crimen no es matar gente,<br> es matar la verdad."'
			  	,'"La corrupción también vende y cobra."'
				,'"La muerte es una inspiración."'
				,'"Tienen ganas de hacer plata<br> con el miedo de la gente."'
			  	,'"Todo el mundo ha arriesgado la vida<br> por un momento de placer y belleza."'
				,'"Creemos en la curiosidad del lector."'
			  	,'"No me interesa darle al lector respuestas,<br> me interesa sembrarle preguntas."'
				,'"En el mundo en que me muevo,<br> a todo mundo se le dice doctor."'
			  	,'"En la institución es importante dar resultados<br> positivos,así sean falsos. ¡Eso paga el sueldo!"'
			  	,'"En este país, mientras usted conozca al que es,<br> usted puede hacer lo que sea. "']
			
				var index = Math.floor(Math.random()*19);
				console.log(index);
				document.getElementById("frase").innerHTML =frases[index];
	

				 // Set the date we're counting down to
			var countDownDate = new Date("Sep 6, 2019 20:00:00").getTime();

			// Update the count down every 1 second
			var x = setInterval(function() {

			  // Get today's date and time
			  var now = new Date().getTime();

			  // Find the distance between now and the count down date
			  var distance = countDownDate - now;

			  // Time calculations for days, hours, minutes and seconds
			  var days = pad( Math.floor(distance / (1000 * 60 * 60*24)), 2);
			 
			 var hours = pad( Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)), 2);
			  var minutes = pad(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),2);
			  var seconds = pad(Math.floor((distance % (1000 * 60)) / 1000),2);

			  // Display the result in the element with id="demo"
			  
			  document.getElementById("counter").innerHTML ="Faltan "+ days+" días, "+ hours + " horas "
			  + minutes + " minutos y " + seconds+" segundos.";
			  
			  // If the count down is finished, write some text
			  if (distance < 0) {
				clearInterval(x);
				
			  }
			}, 1000);
			
			
			function pad(num, size) {
			var s = num+"";
			while (s.length < size) s = "0" + s;
			return s;
			}

	</script>
</html>
