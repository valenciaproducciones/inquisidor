@extends('layout')
@section('styles')
    @yield('styles')
@endsection

@section('content')
					<div class="row menu justify-content-center">
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/almanza"><b>ALMANZA</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/bardot">AURA BARDOT</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/erazo">ANDRÉS ERAZO </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/kodak">KODAK </a></div>
					<div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/gato"><b>El Gato</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/lucia">Lucía</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/jair">Jair </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/restrepo">Simón Restrepo </a></div>
					</div>
					<div class="row ">
						<div class="col-12 col-md-5">
						<p class="text-left big-text  text-center">
						<br>
							ANDRÉS ERAZO <div class="red  big-text  text-center" width="75%" >El poeta de los muertos</div>
						</p>
						
						</div>
						
						<div class="col-12 col-md-7">
						<br>
						
							<img src="/assets/jjg.gif" width="95%"/>
						</div>
					</div>
					<div class= "row">
						<div class="col-12" style="font-size:1.2rem;">
							<br><p>Educado en valores católicos y burgueses. Cree en la escritura como su profesión. Es más bien reservado, prefiere usar el papel para comunicarse. Para él la tecnología no es la vida, pero la usa. La necesidad económica lo lleva a trabajar en el tabloide, y las ganas de escribir y de ser reconocido lo motivan a quedarse, a pesar de las circunstancias. Sufre por los recuerdos de una tragedia que lo persiguen desde hace años; recuerdos que se acentúan con su entrada a El Inquisidor.</p>

							
						</div> 
					</div> 
				
@endsection