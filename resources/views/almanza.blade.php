@extends('layout')
@section('styles')
    @yield('styles')
@endsection

@section('content')
					<div class="row menu justify-content-center">
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/almanza"><b>rOBERTO ALMANZA</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/bardot">AURA BARDOT</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/erazo">ANDRÉS ERAZO </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/kodak">KODAK </a></div>

					<div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/gato"><b>El Gato</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/lucia">Lucía</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/jair">Jair </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/restrepo">Simón Restrepo </a></div>
					</div>
					<div class="row ">
						<div class="col-12 col-md-5">
						<p class="text-left big-text text-center">
							<br>
							<b>ROBERTO ALMANZA</b> <div class="red  big-text text-center" width="75%" >Loco por matar<br>la competencia</div>
						</p>
						 
						</div>
						
						<div class="col-12 col-md-7">
						<br>
						<br>
							<img class="my-auto" src="/assets/ra.gif" width="100%"/>
						</div>
					</div>
					<div class= "row">
						<div class="col-12" style="font-size:1.2rem;">
						<br>
						<br>
						<p>Director y editor de El inquisidor. Él se niega a aceptar la muerte del papel, de las versiones físicas de El Inquisidor, y le ha declarado la guerra a la tecnología y la modernidad. Es un neurótico malgeniado, pero, a la vez, es bueno y paternal con sus empleados. Lleva más de treinta años con El Inquisidor y ha creado una mentira (su Frankenstein) para salvar su periódico y poner en evidencia a “la sociedad del espectáculo” en la que vivimos. </p>
						</div> 
					</div> 
				
@endsection
@section('scripts')
    @yield('scripts')
@endsection