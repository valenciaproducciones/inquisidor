@extends('layout')
@section('styles')
    @yield('styles')
@endsection
	
@section('content')	
<br><br>
	<div class="row text-center" style="font-size:2em;line-height:1em">
	<div class="col-12">
				El Inquisidor está disponible en <a href="https://www.rtvcplay.co/series/el-inquisidor">www.rtvcplay.co</a> de forma gratuita. 
				<div id="video-header" class="mt-5">
					Tráiler:<br><br>
					 <div class="videocontainer mx-auto" width="50%">
						<iframe class="video" src="https://www.youtube.com/embed/xlPM6jDb2cM" frameborder="0" allow="autoplay; fullscreen"></iframe>
					</div>
				</div>
				<br>
					<a href="https://www.rtvcplay.co/series/el-inquisidor/se-acosto-escritor-se-levanto-periodista" target="blank">Mira el Capítulo 1</a>
				<br><br>
				<div id="video-header" class="mt-5">
					Promo capítulo 2:<br><br>
					 <div class="videocontainer mx-auto" width="50%">
						<iframe class="video" src="https://www.youtube.com/embed/JtF-6i0CFY8" frameborder="0" allow="autoplay; fullscreen"></iframe>
					</div>
				</div><br>
					<a href="https://www.rtvcplay.co/series/el-inquisidor/prensa-sangre-entra" target="blank">Mira el Capítulo 2</a>
				
				<div id="video-header" class="mt-5">
					Promo capítulo 3:<br><br>
					 <div class="videocontainer mx-auto" width="50%">
						<iframe class="video" src="https://www.youtube.com/embed/2bqSR03XXd0" frameborder="0" allow="autoplay; fullscreen"></iframe>
					</div>
				</div><br>
					<a href="https://www.rtvcplay.co/series/el-inquisidor/verdad-no-mas-mentira-bien-contada" target="blank">Mira el Capítulo 3</a>
				
					<div id="video-header" class="mt-5">
					Promo capítulo 4:<br><br>
					 <div class="videocontainer mx-auto" width="50%">
						<iframe class="video" src="https://www.youtube.com/embed/1JRm0bNYQrc" frameborder="0" allow="autoplay; fullscreen"></iframe>
					</div>
				</div><br>
					<a href="https://www.rtvcplay.co/series/el-inquisidor/periodista-muchos-huevos" target="blank">Mira el Capítulo 4</a>
				<div id="video-header" class="mt-5">
					Promo capítulo 5:<br><br>
					 <div class="videocontainer mx-auto" width="50%">
						<iframe class="video" src="https://www.youtube.com/embed/PExhst-OpjI" frameborder="0" allow="autoplay; fullscreen"></iframe>
					</div>
				</div><br>
					<a href="https://www.rtvcplay.co/series/el-inquisidor/asesino-mayordomo" target="blank">Mira el Capítulo 5</a>
				<br>
				<br>
				<br>
					<p href="https://www.rtvcplay.co/series/el-inquisidor/" target="blank">Mira el Capítulo 6 este 10 de Octubre</p>
				</div>
				
				
		<div class="col-12" >
		<br>
			
			<br><br>
			
		<br>			
			
				
			
		</div> 
	</div>
	@endsection
	
@section('scripts')

    @yield('scripts')
@endsection