@extends('layout')
@section('styles')
    @yield('styles')
@endsection

@section('content')
					<div class="row menu justify-content-center">
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/almanza"><b>ALMANZA</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/bardot">AURA BARDOT</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/erazo">ANDRÉS ERAZO </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/kodak">KODAK </a></div>
					<div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/gato"><b>El Gato</b></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/lucia">Lucía</a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/jair">Jair </a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/restrepo">Simón Restrepo </a></div>
					</div>
					<div class="row ">
						<div class="col-12 col-md-5">
						<p class="text-left big-text text-center">
						<br>
							EL GATO <div class="red  big-text  text-center" width="75%" ></div>
						</p>
						<br><p style="font-size:1.7em;">Reportero y redactor. Es un pavo real, le gusta mostrarse, es simpático y físicamente atractivo. Puede engatusar a cualquiera hablando, pero detrás de toda su simpatía hay un hombre sin escrúpulos, dispuesto a hacer lo que sea por cumplir sus objetivos personales.</p>
							
						</div>
						
						<div class="col-12 col-md-7">
						<br>
						
							<img src="/assets/g.gif" width="95%"/>
						</div>
					</div>
					<div class= "row">
						<div class="col-12" style="font-size:1.2rem;">
							
							
						</div> 
					</div> 
				
@endsection