@extends('layout')
@section('styles')
    @yield('styles')
@endsection

@section('content')
       
				
					<div class="row bordered">
						
							<p class="col-6 big-text  text-left">
								LA PRIMERA SERIE CINEMATOGRÁFICA DEL MUNDO HECHA <br><span class="red">CON CELULARES</span>
							</p>
							<div class="col-6">
								<img class="" src="/assets/456.gif" width="90%"/>
							</div>
						
					</div>
					<div class= "row ">
						<div class="bordered">
							<div class="col-12" style="font-size:1.2rem;">
								<div class="row red big-text high"> <div class="col  text-center">EL QUE PEGA PRIMERO…</div></div>
								Creatividad, buenas ideas y un celular con cámara, son las únicas condiciones para participar en SmartFilms, el festival de cine al que todos los amantes de los cortometrajes pueden acceder, y que en poco tiempo se ha convertido en uno de los más importantes del país, gracias a la oportunidad que ofrece a sus participantes de exhibir todo tipo de contenidos grabados con celular o dispositivos móviles.
								Este año el festival, siendo consecuente con su promesa de marca, crea, produce y distribuye en asocio con RTVCPlay (plataforma digital de entretenimiento del Sistema de Medios Públicos de Colombia – RTVC) su primera serie original grabada completamente con celulares, tablets, cámaras de computador, cámaras de seguridad y GoPro, y que marca el anhelado regreso del escritor Mauricio Navas al género del thriller.
								“El Inquisidor” es la primera serie hecha con dispositivos móviles en Colombia y el mundo, y su lanzamiento se dará en el marco del festival SmartFilms 2019.
								<br><br>

							</div> 
						</div> 
					</div> 
					<div class= "row ">
						<div class="col-12 col-md-5" style="font-size:1.2rem;">
							<div class="bordered">
								<div class="row red medium-text high"><div class="col  text-center">¡DIOS LOS CRÍA Y LA PRENSA LOS JUNTA!</div></div>
								El joven periodista Andrés Erazo fue invitado a hacer sus prácticas en El Inquisidor. Coincidiendo con su ingreso aparece un asesino en serie que se ensaña con mujeres bellas a las que maquilla y tatúa en la frente un número romano. Aura Bardot, periodista estrella del periódico, y el joven practicante, quedarán unidos por la atracción y por el miedo, pues el asesino está cada vez más cerca.
								<br><br>
							</div>
						</div> 
						
						<div class="col-12 col-md-7" style="font-size:1.2rem;">
							<div class="bordered">
								<div class="row">
									<div class="col-12 col-md-6" style="font-size:1.2rem;">
										<div class="row red medium-text high"><div class="col  text-center">¡NAVAS MONTÓ LA ESCENA… DEL CRIMEN!</div></div>
											Mauricio Navas Talero, con más de 30 años dedicados a la escritura, 4 premios Simón Bolívar, 5 Indias Catalina, 2 premios TVyNovelas y más de 2000 libretos escritos para televisión, regresa al thriller, género que lo llevó a la fama con “La Alternativa del Escorpión” y “La Mujer del Presidente”.	
											<br><br>
									</div> 
									
									<div class="col-12 col-md-5 ">
											<img class="framed" src="/assets/mnt.gif" width="90%"/>
											<span  style="font-size:0.8rem;"><i>Mauricio Navas Talero</i></span>
									</div> 
								</div> 
							</div> 
						</div> 
					</div> 
					
					
					
					<div class= "row ">
						<div class="col-12 col-md-5 " style="font-size:1.2rem;">
							<div class="bordered">
								<div class="row red medium-text high"><div class="col  text-center">¡CON LAS MANOS EN EL MAZO!</div></div>
								Juan Carlos Mazo, ganador del gran SmartFilms 2018, y Andrés Valencia, director artístico de SmartFilms, serán los responsables de dirigir a cuatro manos el apasionante thriller “El Inquisidor”.
								<br><br>
								<img class="framed" src="/assets/jcm.gif" width="90%"/><br>
								<span  style="font-size:0.8rem;"><i>Juan Carlos Mazo</i></span>
							</div> 
						</div> 
						{{--<div class=" col-2 " style="font-size:1.2rem;">
							<div  class="bordered text-center d-none d-md-block">
								<img class="my-auto mx-auto" src="/assets/ap2.png"/>
							</div>
							<div  class="bordered text-center d-md-none">
								<img class="my-auto mx-auto" src="/assets/ap2m.png"/>
							</div>
						</div>--}} 
						<div class="col-12 col-md-7" style="font-size:1.2rem;">
							<div class="bordered">
								<div class="row red medium-text high"><div class="col  text-center">¡CON ESE MAESTRO…!</div></div>
										El thriller periodístico “El Inquisidor” es una idea original del joven escritor colombiano Gustavo Salcedo, alumno aventajado de la Escuela de Escritores de Mauricio Navas Talero, y el multipremiado escritor y libretista Mauricio Navas. 
										<br><br>
										<img class="framed" src="/assets/gs.gif" width="90%"/><br>
										<span  style="font-size:0.8rem;"><i>Gustavo Salcedo</i></span>
							</div> 
						</div> 
					</div>
					
					<div class= "row ">
						<div class="col-12 " style="font-size:1.2rem;">
							<div class="bordered">
								<div class="row">
									<div class="col-12 col-md-5 big-text  text-center">
										<span class="red"> &nbsp;¡CON ESTA&nbsp; </span><br>
										<span class="red-text">VAN NUEVE!</span>
									</div>
									<div class="col-12 col-md-7">
										<img  src="/assets/rtvc.png" width="90%"/><br>
										RTVCPlay, como generador de contenidos de calidad y que abordan temas vigentes y relevantes para las audiencias digitales, en coproducción con SmartFilms lanza su novena serie original que estará disponible completamente gratis en <a href="https://www.rtvcplay.co/series/el-inquisidor">www.rtvcplay.co</a>
									</div>
								</div>
								
							</div> 
						</div> 
					</div>
					<div class="row" style="font-size:1.2rem;">
					{{--<div class="col-12" style="font-size:1.2rem;">
							<div class="bordered text-center  d-none d-md-block">
								<img class="my-auto mx-auto"  src="/assets/ap3.png"/>
							</div>
							<div  class="bordered text-center d-md-none">
								<img class="my-auto mx-auto" src="/assets/ap3m.png"/>
							</div>
					</div>--}} 
					
					</div> 
					<div class= "row ">
						<div class="col-12 " style="font-size:1.2rem;">
							<div class="bordered inverted ">
								<div class="row ">
									<div class="col-12 col-md-8 vCenter">
											
												<div class="medium-text  text-center ">
												¡Descubiertos los “actores” intelectuales!
												</div>
												Se reveló que “El Inquisidor” cuenta con un reparto de ensueño que incluye a: Jorge Enrique Abello, Diego León Hoyos, Cristina Umaña, Luis Eduardo Arango, Rafael Novoa, María Adelaida Puerta y la joven promesa José Julián Gaviria.</p>
										
										
									</div>
									<div class="col-8 col-md-4">
										<img class="framed" src="/assets/jjg.gif" width="90%"/><br>
										<span  style="font-size:0.8rem;"><i>José Julián Gaviria</i></span>
									</div>
								</div>
								
							</div> 
						</div> 
					</div>


   @endsection

@section('scripts')
    @yield('scripts')
@endsection