@extends('layout')
@section('styles')
    @yield('styles')
@endsection

@section('content')
					<div class="row menu justify-content-center">
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/almanza"><b>ROBERTO ALMANZA</b></a><a href="/almanza"><img src="/assets/ra.gif" width="85%"/></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/bardot">AURA BARDOT</a><a href="/bardot"><img src="/assets/ab.gif" width="85%"/></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/erazo">ANDRÉS ERAZO </a><a href="/erazo"><img src="/assets/jjg.gif" width="85%"/></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/kodak">KODAK </a><a href="/kodak"><img src="/assets/k.gif" width="85%"/></a></div>

					<div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/gato"><b>El Gato</b></a><a href="/gato"><img src="/assets/g.gif" width="85%"/></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/lucia">Lucía</a><a href="/lucia"><img src="/assets/l.gif" width="85%"/></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/jair">Jair </a><a href="/jair"><img src="/assets/j.gif" width="85%"/></a></div>
                    <div class = "col-12 col-sm-6 col-md-3 menuitem text-center"><a href="/restrepo">Simón Restrepo </a><a href="/restrepo"><img src="/assets/sr.gif" width="85%"/></a></div>
					</div>
@endsection		
@section('scripts')
    @yield('scripts')
@endsection